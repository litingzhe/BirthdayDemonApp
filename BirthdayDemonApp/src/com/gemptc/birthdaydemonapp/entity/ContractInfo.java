package com.gemptc.birthdaydemonapp.entity;

import android.graphics.Bitmap;

public class ContractInfo {
	
	private String name;
	private String phone;
	private Bitmap bit;
	private boolean ischecked;
	private String sortLetters;
	public ContractInfo(String name, String phone, Bitmap bit, boolean ischecked) {
		super();
		this.name = name;
		this.phone = phone;
		this.bit = bit;
		this.ischecked = ischecked;
	}
	
	public ContractInfo(String name, String phone, Bitmap bit) {
		super();
		this.name = name;
		this.phone = phone;
		this.bit = bit;
	}

	public ContractInfo() {
		// TODO Auto-generated constructor stub
	}

	public String getSortLetters() {
		return sortLetters;
	}

	public void setSortLetters(String sortLetters) {
		this.sortLetters = sortLetters;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Bitmap getBit() {
		return bit;
	}
	public void setBit(Bitmap bit) {
		this.bit = bit;
	}
	public boolean isIschecked() {
		return ischecked;
	}
	public void setIschecked(boolean ischecked) {
		this.ischecked = ischecked;
	}

	public ContractInfo(String name, String phone) {
		super();
		this.name = name;
		this.phone = phone;
	}
	

}
