package com.gemptc.birthdaydemonapp.entity;

import java.io.Serializable;

import net.tsz.afinal.annotation.sqlite.Table;

@Table(name="friendlist")
public class FriendList  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String gender;
	private String groupname;
	private String friendname;
	private String friendphonenum;
	private String friendpic;
	private String  friendbirthday;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public String getFriendname() {
		return friendname;
	}
	public void setFriendname(String friendname) {
		this.friendname = friendname;
	}
	public String getFriendphonenum() {
		return friendphonenum;
	}
	public void setFriendphonenum(String friendphonenum) {
		this.friendphonenum = friendphonenum;
	}
	
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getFriendpic() {
		return friendpic;
	}
	public void setFriendpic(String friendpic) {
		this.friendpic = friendpic;
	}
	public String getFriendbirthday() {
		return friendbirthday;
	}
	public void setFriendbirthday(String friendbirthday) {
		this.friendbirthday = friendbirthday;
	}

	public FriendList() {
		super();
	}
	public FriendList(int id, String gender, String groupname,
			String friendname, String friendphonenum, String friendpic,
			String friendbirthday) {
		super();
		this.id = id;
		this.gender = gender;
		this.groupname = groupname;
		this.friendname = friendname;
		this.friendphonenum = friendphonenum;
		this.friendpic = friendpic;
		this.friendbirthday = friendbirthday;
	}
	public FriendList(String gender, String groupname, String friendname,
			String friendphonenum, String friendpic, String friendbirthday) {
		super();
		this.gender = gender;
		this.groupname = groupname;
		this.friendname = friendname;
		this.friendphonenum = friendphonenum;
		this.friendpic = friendpic;
		this.friendbirthday = friendbirthday;
	}
	
	
	
}
