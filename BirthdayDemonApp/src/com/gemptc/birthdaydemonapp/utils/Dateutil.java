package com.gemptc.birthdaydemonapp.utils;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Dateutil {

	public static long getdays(String date) {
		SimpleDateFormat ft = new SimpleDateFormat("yyyy年MM月dd日");
		String date2 = ft.format(new Date());
		long quot = 0;
		try {
			Date d1 = ft.parse(date);
			Date d2 = ft.parse(date2);
			int count = 0;
			for (int i = d1.getYear() + 1900; i < d2.getYear() + 1900; i++) {

				if ((i % 400 == 0) || (i % 4 == 0) && (i % 100 != 0)) {
					System.out.println("该年是闰年");
					count++;
				} else {
					System.out.println("该年不是闰年");
				}
			}
			quot = d2.getTime() - d1.getTime();
			quot = quot / 1000 / 60 / 60 / 24 - count;
			long result = 365 - quot % 365;
			return result;

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return quot;
	}

	public static int getage(String date) {

		SimpleDateFormat ft = new SimpleDateFormat("yyyy年MM月dd日");
		Date d = null;
		try {
			d = ft.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date dd = new Date();
		int byear = d.getYear();
		int now = dd.getYear();

		return now - byear;
	}

	public static Date getStringtoDate(String date) {

		SimpleDateFormat ft = new SimpleDateFormat("yyyy年MM月dd日");
		Date d = null;
		try {
			d = ft.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}

	public static String getDatetoString(Date d) {
		SimpleDateFormat ft = new SimpleDateFormat("yyyy年MM月dd日");

		return ft.format(d);
	}

	public static long getliveday(String date) {
		SimpleDateFormat ft = new SimpleDateFormat("yyyy年MM月dd日");
		String date2 = ft.format(new Date());
		long quot = 0;

		Date d1;
		try {
			d1 = ft.parse(date);
			Date d2 = ft.parse(date2);

			quot = d2.getTime() - d1.getTime();
			quot = quot / 1000 / 60 / 60 / 24;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return quot;
	}

}