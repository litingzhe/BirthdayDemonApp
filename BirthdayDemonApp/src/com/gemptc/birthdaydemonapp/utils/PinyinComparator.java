package com.gemptc.birthdaydemonapp.utils;

import java.util.Comparator;

import com.gemptc.birthdaydemonapp.entity.ContractInfo;

/**
 * 
 * @author xiaanming
 *
 */
public class PinyinComparator implements Comparator<ContractInfo> {

	public int compare(ContractInfo o1, ContractInfo o2) {
		if (o1.getSortLetters().equals("@")
				|| o2.getSortLetters().equals("#")) {
			return -1;
		} else if (o1.getSortLetters().equals("#")
				|| o2.getSortLetters().equals("@")) {
			return 1;
		} else {
			return o1.getSortLetters().compareTo(o2.getSortLetters());
		}
	}

}
