package com.gemptc.birthdaydemonapp.fragment;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.gemptc.birthdaydemonapp.R;
import com.gemptc.birthdaydemonapp.activity.AddBirthdaywayActivity;
import com.gemptc.birthdaydemonapp.activity.AddbirthadyActivity;
import com.gemptc.birthdaydemonapp.activity.FriendListActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

public class BirthdayFragment extends Fragment implements OnItemSelectedListener,
OnItemClickListener {
	private GridView gv;
	private LayoutInflater inflater;
	private int[] imageRes = { R.drawable.a7, R.drawable.a1,
			R.drawable.a15, R.drawable.a9, R.drawable.a12, R.drawable.a11,
			R.drawable.a8, R.drawable.a13, R.drawable.a16,R.drawable.addbirthday_item};
	// 定义标题数组
	private String[] itemName = {"全部生日","亲人","朋友","同学","同事",
			"领导","客户","合作伙伴","未分组","添加好友"};
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.birthday_fragment, container,
				false);
	   GridView gridview=(GridView) view.findViewById(R.id.grid);
		
		List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
		int length = itemName.length;
		for (int i = 0; i < length; i++) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ItemImageView", imageRes[i]);
			map.put("ItemTextView", itemName[i]);
			data.add(map);
		}
		SimpleAdapter simpleAdapter =new SimpleAdapter(this.getActivity(), data, 
				R.layout.gridview_item, new String[] { "ItemImageView",
				"ItemTextView" }, new int[] { R.id.Itemiv,
				R.id.Itemtv });
		gridview.setAdapter(simpleAdapter);
		
		gridview.setOnItemSelectedListener(this);
		gridview.setOnItemClickListener(this);
		return view;
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int poistion, long id) {
		if(poistion==9){
			
			Intent intent=new Intent(getActivity(),AddBirthdaywayActivity.class);
			Bundle b=new Bundle();
			TextView group =(TextView) view.findViewById(R.id.Itemtv);
			String flag=group.getText().toString().trim();
			b.putString("flag", flag);
			intent.putExtras(b);
			startActivity(intent);
		}
		else{
			Intent intent=new Intent(getActivity(),FriendListActivity.class);
			Bundle b=new Bundle();
			TextView group =(TextView) view.findViewById(R.id.Itemtv);
			String flag=group.getText().toString().trim();
			b.putString("flag", flag);
			intent.putExtras(b);
			startActivity(intent);
			
		}
		
	}
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int poistion,
			long id) {
		
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	
}
