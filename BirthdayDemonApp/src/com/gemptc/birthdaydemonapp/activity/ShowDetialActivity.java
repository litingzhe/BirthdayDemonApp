package com.gemptc.birthdaydemonapp.activity;

import com.gemptc.birthdaydemonapp.R;
import com.gemptc.birthdaydemonapp.entity.FriendList;
import com.gemptc.birthdaydemonapp.utils.Dateutil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ShowDetialActivity extends ActionBarActivity implements OnClickListener{
	private ImageView sendmsm;
	private ImageView callphone;
	private ImageView sendcard;
	private ImageView detial_iv_photo;
	private TextView detial_tv_friendname;
	private ImageView gender;
	private TextView detial_tv_friendbirthday;
	private TextView detial_info;
	private TextView detial_days;
	private TextView b_n_day;
	private FriendList f;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.frienddetial);
		ActionBar actionBar = getSupportActionBar();
		getSupportActionBar().setTitle("详细信息");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_bg));
		actionBar.show();
		initview();
		sendmsm=(ImageView) findViewById(R.id.sendmsm);
		callphone=(ImageView) findViewById(R.id.callphone);
		sendcard=(ImageView) findViewById(R.id.sendcard);
		sendmsm.setOnClickListener(this);
		callphone.setOnClickListener(this);
		sendcard.setOnClickListener(this);
	}

	private void initview() {
		Bundle b=getIntent().getExtras();
	      f= (FriendList) b.getSerializable("friendinfo");
		System.out.println(f.getFriendbirthday()+"dddddddddd");
		String birthday=f.getFriendbirthday();
		int age=Dateutil.getage(birthday);
		 long days=Dateutil.getdays(birthday);
		 long livedays=Dateutil.getliveday(birthday);
		 detial_iv_photo=(ImageView) findViewById(R.id.detial_iv_photo);
		 detial_tv_friendname=(TextView) findViewById(R.id.detial_tv_friendname);
		 gender=(ImageView) findViewById(R.id.gender);
		 detial_tv_friendbirthday=(TextView) findViewById(R.id.detial_tv_friendbirthday);
		 detial_info=(TextView) findViewById(R.id.detial_info);
		 detial_days=(TextView) findViewById(R.id.detial_days);
		 b_n_day=(TextView) findViewById(R.id.b_n_day);
		 Uri uri = Uri.parse(f.getFriendpic());
		 detial_iv_photo.setImageURI(uri);
		 detial_tv_friendname.setText(f.getFriendname());
		 System.out.println("dddddddddddddddddd"+f.getGender());
		 if(f.getGender()!=null){
		 if(f.getGender().equals("男")){
			 gender.setImageResource(R.drawable.genderman);
		 }
		 else{
					gender.setImageResource(R.drawable.genderwoman); 
		 }
		 }
		 else{
			 gender.setVisibility(View.GONE);
			
		 }
		 detial_tv_friendbirthday.setText("生日"+f.getFriendbirthday());
		 detial_info.setText("距他"+age+"岁生日还有");
		 detial_days.setText(Long.toString(days));
		 b_n_day.setText(Long.toString(livedays));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.sendmsm:
			sendmsm.setBackgroundResource(R.drawable.message_after);
			Intent intent=new Intent(this,SendMessageActivity.class);
			startActivity(intent);
			break;
			
		case R.id.callphone:
			callphone.setBackgroundResource(R.drawable.phone_after);
			if(f.getFriendphonenum()!=null&&f.getFriendphonenum()!=""){
				Intent callIn = new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+f.getFriendphonenum())); 
				 this.startActivity(callIn);
				 break;
					
				
			}
			
			
		case R.id.sendcard:
			
			sendcard.setBackgroundResource(R.drawable.cardafter);
			break;
		}
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.feindendetialmenu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id= item.getItemId();
		if(id==R.id.modify){
			
			Intent intent = new Intent(this,
					ModifyFriend.class);
			Bundle b = new Bundle();
			System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaa"+f.getFriendbirthday());
			b.putSerializable("friendinfo", f);
			intent.putExtras(b);
			startActivity(intent);
			
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		initview();
		super.onRestart();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		onRestart();
		super.onResume();
	}
	
}
