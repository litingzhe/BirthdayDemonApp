package com.gemptc.birthdaydemonapp.activity;

import net.tsz.afinal.FinalDb;

import com.gemptc.birthdaydemonapp.R;
import com.gemptc.birthdaydemonapp.fragment.BirthdayFragment;
import com.gemptc.birthdaydemonapp.fragment.BlessFragment;
import com.gemptc.birthdaydemonapp.fragment.MeFragment;
import com.gemptc.birthdaydemonapp.fragment.MessageFragment;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
public class MainActivity extends ActionBarActivity {
  ActionBar actionbar;
	private static FragmentManager fMgr;

	private RadioButton birthday;
	private RadioButton message;
	private RadioButton blesswall;
	private RadioButton me;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		actionbar=getActionBar();
		actionbar.hide();
		birthday = (RadioButton) findViewById(R.id.birtthday);
		message = (RadioButton) findViewById(R.id.message);
		blesswall = (RadioButton) findViewById(R.id.blesswall);
		me = (RadioButton) findViewById(R.id.me);
		// 获取FragmentManager实例
		fMgr = getSupportFragmentManager();

		initFragment();
		dealBottomButtonsClickEvent();

	}

	/**
	 * 初始化首个Fragment
	 */
	private void initFragment() {
		FragmentTransaction ft = fMgr.beginTransaction();
		BirthdayFragment birthdayFragment = new BirthdayFragment();
		ft.add(R.id.fragmentRoot, birthdayFragment, "birthdayFragment");
		ft.addToBackStack("birthdayFragment");
		ft.commit();
	}

	/**
	 * 处理底部点击事件
	 */
	@SuppressLint("NewApi")
	private void dealBottomButtonsClickEvent() {

		findViewById(R.id.birtthday).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (fMgr.findFragmentByTag("birthdayFragment") != null
						&& fMgr.findFragmentByTag("birthdayFragment")
								.isVisible()) {

					return;
				}
				popAllFragmentsExceptTheBottomOne();

			}
		});
		findViewById(R.id.message).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popAllFragmentsExceptTheBottomOne();
				FragmentTransaction ft = fMgr.beginTransaction();
				ft.hide(fMgr.findFragmentByTag("birthdayFragment"));
				MessageFragment mf = new MessageFragment();
				ft.add(R.id.fragmentRoot, mf, "MessageFragment");
				ft.addToBackStack("MessageFragment");
				ft.commit();

			}
		});
		findViewById(R.id.blesswall).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popAllFragmentsExceptTheBottomOne();
				FragmentTransaction ft = fMgr.beginTransaction();
				ft.hide(fMgr.findFragmentByTag("birthdayFragment"));
				BlessFragment bf = new BlessFragment();
				ft.add(R.id.fragmentRoot, bf, "BlessFragment");
				ft.addToBackStack("BlessFragment");
				ft.commit();
			}
		});
		findViewById(R.id.me).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				popAllFragmentsExceptTheBottomOne();
				FragmentTransaction ft = fMgr.beginTransaction();
				ft.hide(fMgr.findFragmentByTag("birthdayFragment"));
				MeFragment sf = new MeFragment();
				ft.add(R.id.fragmentRoot, sf, "MeFragment");
				ft.addToBackStack("MeFragment");
				ft.commit();
			}
		});
	}

	/**
	 * 从back stack弹出所有的fragment，保留首页的那个
	 */
	public static void popAllFragmentsExceptTheBottomOne() {
		for (int i = 0, count = fMgr.getBackStackEntryCount() - 1; i < count; i++) {
			fMgr.popBackStack();
		}
	}

	// 点击返回按钮
	@Override
	public void onBackPressed() {
		popAllFragmentsExceptTheBottomOne();

		if (fMgr.findFragmentByTag("birthdayFragment") != null
				&& fMgr.findFragmentByTag("birthdayFragment").isVisible()) {
			MainActivity.this.finish();
		} else {
			super.onBackPressed();
		}
	}
}