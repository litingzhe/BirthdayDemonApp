package com.gemptc.birthdaydemonapp.activity;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.tsz.afinal.FinalDb;

import com.gemptc.birthdaydemonapp.R;
import com.gemptc.birthdaydemonapp.entity.ContractInfo;
import com.gemptc.birthdaydemonapp.entity.FriendList;
import com.gemptc.birthdaydemonapp.utils.CharacterParser;
import com.gemptc.birthdaydemonapp.utils.ClearEditText;
import com.gemptc.birthdaydemonapp.utils.PinyinComparator;
import com.gemptc.birthdaydemonapp.utils.SideBar;
import com.gemptc.birthdaydemonapp.utils.SideBar.OnTouchingLetterChangedListener;
import com.gemptc.birthdaydemonapp.utils.SortAdapter;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class addFriendContractList extends ActionBarActivity {
	private CharacterParser characterParser;
	private PinyinComparator pinyinComparator;
	Context mContext = null;
	private SideBar sideBar;
	private TextView dialog;
	private FinalDb db;
	private ClearEditText mClearEditText;   
	/** 获取库Phon表字段 **/
	private static final String[] PHONES_PROJECTION = new String[] {
			Phone.DISPLAY_NAME, Phone.NUMBER, Photo.PHOTO_ID, Phone.CONTACT_ID };
	/** 联系人显示名称 **/
	private static final int PHONES_DISPLAY_NAME_INDEX = 0;
	/** 电话号码 **/
	private static final int PHONES_NUMBER_INDEX = 1;
	/** 头像ID **/
	private static final int PHONES_PHOTO_ID_INDEX = 2;
	/** 联系人的ID **/
	private static final int PHONES_CONTACT_ID_INDEX = 3;
	private ArrayList<ContractInfo> data;
	private ListView lv;
	private SortAdapter adapter;
	private ArrayList<ContractInfo> news;
	private Resources r;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mContext = this;
		super.onCreate(savedInstanceState);
		r=getResources();
		setContentView(R.layout.contractactivity);
		ActionBar actionBar = getSupportActionBar();
		getSupportActionBar().setTitle("联系人");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_bg));
		actionBar.show();
		 news=new ArrayList<ContractInfo>();
		characterParser = CharacterParser.getInstance();
		pinyinComparator = new PinyinComparator();
		sideBar = (SideBar) findViewById(R.id.sidrbar);
		dialog = (TextView) findViewById(R.id.dialog);
		sideBar.setTextView(dialog);
		  sideBar.setOnTouchingLetterChangedListener(new OnTouchingLetterChangedListener() {
				
				@Override
				public void onTouchingLetterChanged(String s) {
					//该字母首次出现的位置
					int position = adapter.getPositionForSection(s.charAt(0));
					if(position != -1){
						lv.setSelection(position);
					}
					
				}
			});
		
		lv = (ListView) findViewById(R.id.contractlist);
		data = new ArrayList<ContractInfo>();
		getPhoneContacts();
		Collections.sort(data, pinyinComparator);
		adapter = new SortAdapter(this,data);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {
         int i=1;
         
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TextView name = (TextView) view
						.findViewById(R.id.color_title);
				TextView phonenum = (TextView) view
						.findViewById(R.id.color_text);
				final String conname=(String) name.getText();
				final String phone=(String) phonenum.getText();
			    Toast.makeText(addFriendContractList.this, "ok", Toast.LENGTH_LONG).show();
			    CheckBox cb =(CheckBox) view.findViewById(R.id.cbx1);
			   boolean flag=cb.isChecked();
			   ContractInfo c=new ContractInfo(conname,phone);
			    if(flag){
			    	news.remove(c);
			    	cb.setChecked(false);
			    }else{
			    	news.add(c);
			    	cb.setChecked(true);
			    
			    }
			}
		});
		
		mClearEditText = (ClearEditText) findViewById(R.id.filter_edit);
		//根据输入框输入值的改变来过滤搜索
		mClearEditText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				//当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
				filterData(s.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	
	/**得到手机通讯录联系人信息**/
    private void getPhoneContacts() {
	ContentResolver resolver = mContext.getContentResolver();

	// 获取手机联系人
	Cursor phoneCursor = resolver.query(Phone.CONTENT_URI,PHONES_PROJECTION, null, null, null);


	if (phoneCursor != null) {
	    while (phoneCursor.moveToNext()) {

		//得到手机号码
		String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX);
		//当手机号码为空的或者为空字段 跳过当前循环
		if (TextUtils.isEmpty(phoneNumber))
		    continue;
		
		//得到联系人名称
		String contactName = phoneCursor.getString(PHONES_DISPLAY_NAME_INDEX);
		
		//得到联系人ID
		Long contactid = phoneCursor.getLong(PHONES_CONTACT_ID_INDEX);

		//得到联系人头像ID
		Long photoid = phoneCursor.getLong(PHONES_PHOTO_ID_INDEX);
		
		//得到联系人头像Bitamp
		Bitmap contactPhoto = null;

		//photoid 大于0 表示联系人有头像 如果没有给此人设置头像则给他一个默认的
		if(photoid > 0 ) {
		    Uri uri =ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI,contactid);
		    InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(resolver, uri);
		    contactPhoto = BitmapFactory.decodeStream(input);
		}else {
		    contactPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.contact_photo);
		}
		ContractInfo c=new ContractInfo();
		c.setName(contactName);
		c.setPhone(phoneNumber);
		c.setBit(contactPhoto);
		String pinyin = characterParser.getSelling(contactName);
		String sortString = pinyin.substring(0, 1).toUpperCase();
		
		// 正则表达式，判断首字母是否是英文字母
		if(sortString.matches("[A-Z]")){
			c.setSortLetters(sortString.toUpperCase());
		}else{
			c.setSortLetters("#");
		}
		data.add(c);
	    }

	    phoneCursor.close();
	}
    }
    
    /**得到手机SIM卡联系人人信息**/
    private void getSIMContacts() {
	ContentResolver resolver = mContext.getContentResolver();
	// 获取Sims卡联系人
	Uri uri = Uri.parse("content://icc/adn");
	Cursor phoneCursor = resolver.query(uri, PHONES_PROJECTION, null, null,
		null);

	if (phoneCursor != null) {
	    while (phoneCursor.moveToNext()) {

		// 得到手机号码
		String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX);
		// 当手机号码为空的或者为空字段 跳过当前循环
		if (TextUtils.isEmpty(phoneNumber))
		    continue;
		// 得到联系人名称
		String contactName = phoneCursor
			.getString(PHONES_DISPLAY_NAME_INDEX);

		//Sim卡中没有联系人头像
		Bitmap contactPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.contact_photo);
		ContractInfo c=new ContractInfo();
		c.setBit(contactPhoto);
		c.setName(contactName);
		c.setPhone(phoneNumber);
		data.add(c);
		
	    }

	    phoneCursor.close();
	}
    }
//	private List<ContractInfo> filledData(String [] date){
//		List<ContractInfo> mSortList = new ArrayList<ContractInfo>();
//		
//		for(int i=0; i<date.length; i++){
//			ContractInfo contractinfo = new ContractInfo();
//			contractinfo.setName(date[i]);
//			//汉字转换成拼音
//			String pinyin = characterParser.getSelling(date[i]);
//			String sortString = pinyin.substring(0, 1).toUpperCase();
//			
//			// 正则表达式，判断首字母是否是英文字母
//			if(sortString.matches("[A-Z]")){
//				contractinfo.setSortLetters(sortString.toUpperCase());
//			}else{
//				contractinfo.setSortLetters("#");
//			}
//			
//			mSortList.add(contractinfo);
//		}
//		return mSortList;
//		
//	}
//	
	/**
	 * 根据输入框中的值来过滤数据并更新ListView
	 * @param filterStr
	 */
	private void filterData(String filterStr){
		List<ContractInfo> filterDateList = new ArrayList<ContractInfo>();
		
		if(TextUtils.isEmpty(filterStr)){
			filterDateList = data;
		}else{
			filterDateList.clear();
			for(ContractInfo sortModel : data){
				String name = sortModel.getName();
				if(name.indexOf(filterStr.toString()) != -1 || characterParser.getSelling(name).startsWith(filterStr.toString())){
					filterDateList.add(sortModel);
				}
			}
		}
		
		// 根据a-z进行排序
		Collections.sort(filterDateList, pinyinComparator);
		adapter.updateListView(filterDateList);
	}

	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.addbycontract, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id= item.getItemId();
		if(id==R.id.addc){
			
			
			  Toast.makeText(addFriendContractList.this, "aaaaaok", Toast.LENGTH_LONG).show();
			db = FinalDb.create(this, "mydb.db");
			for(int i=0;i<news.size();i++){
				FriendList f=new FriendList();
				f.setFriendname(news.get(i).getName());
				f.setFriendphonenum(news.get(i).getPhone());
				Uri uri = Uri
						.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
								+ "://"
								+ r.getResourcePackageName(R.drawable.default_avatar_grey)
								+ "/"
								+ r.getResourceTypeName(R.drawable.default_avatar_grey)
								+ "/"
								+ r.getResourceEntryName(R.drawable.default_avatar_grey));
				f.setFriendpic(uri.toString().trim()); 
			    f.setGroupname("未分组");
				System.out.println(news.get(i).getName()+"dddddddddddddddd");
				db.save(f);
				this.finish();
				
			}
			
		}
		return super.onOptionsItemSelected(item);
		
		
		
	}
}


