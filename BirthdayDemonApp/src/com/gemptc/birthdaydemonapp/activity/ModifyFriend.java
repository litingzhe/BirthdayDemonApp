package com.gemptc.birthdaydemonapp.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

import net.tsz.afinal.FinalDb;

import com.gemptc.birthdaydemonapp.R;
import com.gemptc.birthdaydemonapp.entity.FriendList;
import com.gemptc.birthdaydemonapp.utils.Arrayutils;
import com.gemptc.birthdaydemonapp.utils.BitmapUtil;
import com.gemptc.birthdaydemonapp.utils.Tools;

import android.app.AlertDialog;
import android.app.Application;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ModifyFriend extends ActionBarActivity implements
		OnClickListener {
	private static final int CONTACT_REQUEST_CODE = 5;
	private ImageView image_mask;
	private ImageView image_photo;
	private EditText add_et_name;
	private ImageButton im_but_1;
	private RadioButton gender_man;
	private RadioButton gender_woman;
	private TextView Birthday;
	private Spinner spinner;
	private EditText add_et_phone;
	private LinearLayout ly;
	private static final int IMAGE_REQUEST_CODE = 0;
	private static final int CAMERA_REQUEST_CODE = 1;
	private static final int RESULT_REQUEST_CODE = 2;
	private String friendname;
	private String groupname;
	private String friendphonenum;
	private String friendpic;
	private String friendbirthday;
	private String gender;
	private Bitmap photo;
	private Resources r;
	private FinalDb db;
	private FriendList fr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addfriendbirtday);
		ActionBar actionBar = getSupportActionBar();
		getSupportActionBar().setTitle("修改好友");
		r = getResources();
		db = FinalDb.create(this, "mydb.db");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.actionbar_bg));
		actionBar.show();
	   initview();
	
		image_mask.setOnClickListener(this);
		im_but_1.setOnClickListener(this);
		Birthday.setOnClickListener(this);

	}

	private void initview() {
		Intent	intent=getIntent();
		  Bundle b=intent.getExtras();
		  fr=	(FriendList) b.getSerializable("friendinfo");
		 System.out.println(fr.getFriendbirthday()+fr.getFriendname());
			image_mask = (ImageView) findViewById(R.id.image_mask);
			image_photo = (ImageView) findViewById(R.id.image_photo);
			add_et_name = (EditText) findViewById(R.id.add_et_name);
			add_et_phone = (EditText) findViewById(R.id.add_et_phone);
			im_but_1 = (ImageButton) findViewById(R.id.im_but_1);
			gender_man = (RadioButton) findViewById(R.id.gender_man);
			gender_woman = (RadioButton) findViewById(R.id.gender_woman);
			Birthday = (TextView) findViewById(R.id.Birthday);
			spinner = (Spinner) findViewById(R.id.spinner);
			ly=(LinearLayout) findViewById(R.id.linearLayout5);
			Uri uri = Uri.parse(fr.getFriendpic());
			image_photo.setImageURI(uri);
			add_et_name.setText(fr.getFriendname());
			add_et_phone.setText(fr.getFriendphonenum());
			if(fr.getGender()!=null){
			if(fr.getGender().equals("男")){
				gender_man.setChecked(true);
				gender_woman.setChecked(false);
			}
			else{
				gender_woman.setChecked(true);
				gender_man.setChecked(false);
			}}
			else{
				gender_woman.setChecked(false);
				gender_man.setChecked(false);
			}
			String[] groups=getResources().getStringArray(R.array.groups);
			int po=Arrayutils.getArrposition(groups, fr.getGroupname());
			spinner.setSelection(po);
			Birthday.setText(fr.getFriendbirthday());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.add) {
			// Toast.makeText(this, "addok", Toast.LENGTH_LONG).show();
			if (add_et_name.getText().toString().trim().equals("")) {
				Toast.makeText(this, "请输入姓名", Toast.LENGTH_LONG).show();
			}
			if (add_et_phone.getText().toString().trim().equals("")) {
				Toast.makeText(this, "请输入电话", Toast.LENGTH_LONG).show();

			}
			if (Birthday.getText().toString().trim().equals("")) {
				Toast.makeText(this, "请选择生日", Toast.LENGTH_LONG).show();
			}

			else {
				friendname = add_et_name.getText().toString().trim();
				friendphonenum = add_et_phone.getText().toString().trim();
				groupname = spinner.getSelectedItem().toString();
				if (gender_man.isChecked()) {
					gender = "男";
				} if(gender_woman.isChecked()){
					gender = "女";}
				System.out.println(gender);
				System.out.println(friendname + friendphonenum + groupname);
				friendbirthday = Birthday.getText().toString().trim();
				if (photo == null) {
					Toast.makeText(this, "没照片", Toast.LENGTH_LONG).show();
					Uri uri = Uri
							.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
									+ "://"
									+ r.getResourcePackageName(R.drawable.default_avatar_grey)
									+ "/"
									+ r.getResourceTypeName(R.drawable.default_avatar_grey)
									+ "/"
									+ r.getResourceEntryName(R.drawable.default_avatar_grey));
					friendpic = uri.toString().trim();
				}

				else {

					try {
						Bitmap bitmap = photo;
						File f = new File(this.getCacheDir().toString(),
								friendname + friendbirthday + ".png");
						f.createNewFile();
						// Convert bitmap to byte array
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.PNG,
								0 /* ignored for PNG */, bos);
						byte[] bitmapdata = bos.toByteArray();

						// write the bytes in file
						FileOutputStream fos;
						fos = new FileOutputStream(f);
						fos.write(bitmapdata);

						friendpic = Uri.fromFile(f).toString();

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch blockF
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				FriendList f=new FriendList(gender, groupname, friendname,
						friendphonenum, friendpic, friendbirthday);
				db.update(f,"id="+fr.getId());
				db.save(f);
				Intent in=new Intent(this,ShowDetialActivity.class);
				Bundle b=new Bundle();
				b.putSerializable("friendinfo", f);
				in.putExtras(b);
				startActivity(in);
				
			}

			
			
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.photocontext, menu);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.image_photo:
			registerForContextMenu(v);
			ShowDialog();
			break;
		case R.id.im_but_1:
			Toast.makeText(this, "ok", Toast.LENGTH_LONG).show();
			Intent intent = new Intent(this, ContractList.class);
			startActivityForResult(intent, CONTACT_REQUEST_CODE);

			break;
		case R.id.Birthday:
			Calendar c = Calendar.getInstance();
			new DatePickerDialog(ModifyFriend.this,
					new DatePickerDialog.OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker dp, int year,
								int month, int day_of_month) {
							Birthday.setText(year + "年" + month + "月"
									+ day_of_month + "日");
						}
					}, c.get(Calendar.YEAR), c.get(Calendar.MONTH),
					c.get(Calendar.DAY_OF_MONTH)).show();
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode != RESULT_CANCELED) {
			switch (requestCode) {

			case CONTACT_REQUEST_CODE:
				if (resultCode == RESULT_OK) {
					String conname = null;
					String phone = null;
					Bundle bundle = data.getExtras();
					if (bundle != null) {
						conname = bundle.getString("conname");
						phone = bundle.getString("phone");
					}
					add_et_name.setText(conname);
					add_et_phone.setText(phone);
				}
				break;

			case IMAGE_REQUEST_CODE:
				startPhotoZoom(data.getData());
				break;
			// 如果是调用相机拍照时
			case CAMERA_REQUEST_CODE:
				File temp = new File(Environment.getExternalStorageDirectory()
						+ "/headicon" + "/xiaoma.jpg");
				startPhotoZoom(Uri.fromFile(temp));
				break;
			// 取得裁剪后的图片
			case RESULT_REQUEST_CODE:

				if (data != null) {
					setPicToView(data);
				}
				break;
			}
		}
	}

	private void setPicToView(Intent picdata) {
		Bundle extras = picdata.getExtras();
		if (extras != null) {
			photo = extras.getParcelable("data");
			image_photo.setImageBitmap(photo);
		}

	}

	public void startPhotoZoom(Uri uri) {

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 150);
		intent.putExtra("outputY", 150);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, RESULT_REQUEST_CODE);
	}

	private void ShowDialog() {
		new AlertDialog.Builder(this).setItems(new String[] { "相册", "拍照" },
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						switch (which) {
						case 0:
							Intent intentFromGallery = new Intent();
							intentFromGallery.setType("image/*"); // 设置文件类型
							intentFromGallery
									.setAction(Intent.ACTION_GET_CONTENT);
							startActivityForResult(intentFromGallery,
									IMAGE_REQUEST_CODE);
							break;
						case 1:
							Toast.makeText(ModifyFriend.this, "pho",
									Toast.LENGTH_SHORT).show();
							Intent intentFromCapture = new Intent(
									MediaStore.ACTION_IMAGE_CAPTURE);
							// 判断存储卡是否可以用，可用进行存储
							if (Tools.hasSdcard()) {

								intentFromCapture.putExtra(
										MediaStore.EXTRA_OUTPUT,
										Uri.fromFile(new File(Environment
												.getExternalStorageDirectory(),
												UUID.randomUUID() + ".jpg")));
							}

							startActivityForResult(intentFromCapture,
									CAMERA_REQUEST_CODE);
							break;

						}
					}
				}).show();

	}
@Override
protected void onRestart() {
	// TODO Auto-generated method stub
	super.onRestart();
}
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
}
}
