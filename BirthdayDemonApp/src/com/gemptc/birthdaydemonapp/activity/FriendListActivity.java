package com.gemptc.birthdaydemonapp.activity;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import net.tsz.afinal.FinalBitmap;
import net.tsz.afinal.FinalDb;

import com.gemptc.birthdaydemonapp.R;
import com.gemptc.birthdaydemonapp.BirthdayCfg.BirthdayCfg;
import com.gemptc.birthdaydemonapp.entity.FriendList;
import com.gemptc.birthdaydemonapp.utils.Dateutil;

import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class FriendListActivity extends ActionBarActivity {

	private ListView firendlist;
	private ArrayList<FriendList> data;
	private FinalDb fd;
	private MyAdapter adapter;
	private String flag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friendlistactivity);
		Intent intent = this.getIntent(); // 获取已有的intent对象
		Bundle bundle = intent.getExtras(); // 获取intent里面的bundle对象
		flag = bundle.getString("flag"); // 获取Bundle里面的字符串 );
		ActionBar actionBar = getSupportActionBar();
		getSupportActionBar().setTitle(flag);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.actionbar_bg));
		actionBar.show();

		data = new ArrayList<FriendList>();
		Resources r = getResources();
		Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
				+ r.getResourcePackageName(R.drawable.png1) + "/"
				+ r.getResourceTypeName(R.drawable.png1) + "/"
				+ r.getResourceEntryName(R.drawable.png1));
		String url = uri.toString();
		// FriendList f1 = new FriendList(1, 1, "好友", "李挺哲", "133333333",
		// "dd@dad.com", url, "1991年9月18日");
		// FriendList f2 = new FriendList(1, 1, "好友", "李挺哲", "133333333",
		// "dd@dad.com",url, "1991年9月18日");
		// FriendList f3 = new FriendList(1, 1, "好友", "李挺哲", "133333333",
		// "dd@dad.com", url, "1991年9月18日");
		// data.add(f1);
		// data.add(f2);
		// data.add(f3);
		firendlist = (ListView) findViewById(R.id.friendlist);
		adapter = new MyAdapter(data);

		initListData(flag);

		firendlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int poistion, long id) {
				System.out.println(data.get(poistion).getFriendbirthday()+"dadadad");
				if(!(data.get(poistion).getFriendbirthday()==null)){
				Intent intent = new Intent(FriendListActivity.this,
						ShowDetialActivity.class);
				Bundle b = new Bundle();
				FriendList f = data.get(poistion);
				System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaa"+f.getFriendbirthday());
				b.putSerializable("friendinfo", f);
				intent.putExtras(b);
				startActivity(intent);}
				else{
				    final FriendList f = data.get(poistion);
					Calendar c = Calendar.getInstance();
					new DatePickerDialog(FriendListActivity.this,
							new DatePickerDialog.OnDateSetListener() {
								@Override
								public void onDateSet(DatePicker dp, int year,
										int month, int day_of_month) {
								final	String b=year + "年" + month + "月"
											+ day_of_month + "日";
								f.setFriendbirthday(b);
								fd.update(f, "id="+f.getId());
								onRestart();
								}
							}, c.get(Calendar.YEAR), c.get(Calendar.MONTH),
							c.get(Calendar.DAY_OF_MONTH)).show();
				}

			}
		});
	  
	}

	private void initListData(String flag) {
		if (flag.equals("全部生日")) {

			fd = FinalDb.create(this, BirthdayCfg.DB_NAME);
			data.clear();
			data.addAll(fd.findAll(FriendList.class));
			firendlist.setAdapter(adapter);
			adapter.notifyDataSetChanged();

		} else {
			fd = FinalDb.create(this, BirthdayCfg.DB_NAME);
			data.clear();
			// List l= ;
			data.addAll(fd.findAllByWhere(FriendList.class, "groupname=" + "'"
					+ flag + "'"));
			firendlist.setAdapter(adapter);
			adapter.notifyDataSetChanged();
		}
	}

	class MyAdapter extends BaseAdapter {
		private ArrayList<FriendList> data;

		public ArrayList<FriendList> getData() {
			return data;
		}

		public MyAdapter(ArrayList<FriendList> data) {
			super();
			this.data = data;
		}

		public void setData(ArrayList<FriendList> data) {
			this.data = data;
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView friendphoto;
			ImageView cake;
			TextView name;
			TextView birthday;
			TextView info;
			TextView days;
			if (convertView == null) {
				convertView = LayoutInflater.from(FriendListActivity.this)
						.inflate(R.layout.friendlistitem, null);
			}
			friendphoto = (ImageView) convertView
					.findViewById(R.id.friend_photo);
			cake = (ImageView) convertView.findViewById(R.id.bir_cake);
			name = (TextView) convertView.findViewById(R.id.friendname);
			birthday = (TextView) convertView.findViewById(R.id.friendbirthday);
			info = (TextView) convertView.findViewById(R.id.info);
			days = (TextView) convertView.findViewById(R.id.days);
			FriendList f = data.get(position);
			cake.setImageResource(R.drawable.cake);
			name.setText(f.getFriendname());
			String d = f.getFriendbirthday();
			if(d==null||d.equals("")){
				info.setText("未知");
				days.setText("未知");
				birthday.setText("未知");
				
			}else{
				birthday.setText(d);

			long day = Dateutil.getdays(d);
			int age = Dateutil.getage(d);
			String infod = "";
			String dayss = "";
			dayss = day + "天";
			infod = "后过" + age + "岁生日";
			info.setText(infod);
			days.setText(dayss);}
			if(f.getFriendpic()!=null||f.getFriendpic().equals("")){
			Uri uri = Uri.parse(f.getFriendpic());
			friendphoto.setImageURI(uri);
			}
			else{
				friendphoto.setImageResource(R.drawable.item8);
				
			}
			return convertView;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.friendlistmenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();

		if (id == R.id.addfri) {
			Intent in = new Intent(this, AddBirthdaywayActivityagin.class);
			Bundle b = new Bundle();
			b.putString("flag", flag);
			in.putExtras(b);
			startActivity(in);
		}
		return super.onOptionsItemSelected(item);

	}
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	initListData(flag);
	super.onResume();
}
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		initListData(flag);
		super.onRestart();
		
	}
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.addbirt, menu);
	}
	
}
